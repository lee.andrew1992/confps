#include <iostream>
#include "ConFloor.h"
#include <filesystem>
#include <codecvt>
#include <cwchar>
#include <fstream>
#include <locale>
#include <string>
#pragma once

using namespace filesystem;

class Stage
{
public:
	ConFloor* m_cfBaseFloor;

	Map floorMaps[3];

	Stage(filesystem::directory_entry stageDataEntry) {
		int mapIndex = 0;
		wchar_t line[1024];
		wifstream stageMapFile(stageDataEntry);
		int nMapHeight = 0;

		while (stageMapFile.getline(line, 1024))
		{
			// TODO FIX THIS
			if (wcscmp(line, L"") == 0)
			{
				floorMaps[mapIndex].m_nHeight = nMapHeight;
				nMapHeight = 0;
				mapIndex++;
				continue;
			}
			else
			{
				if (floorMaps[mapIndex].m_nWidth == 0)
				{
					floorMaps[mapIndex].m_nWidth = wcslen(line);
				}
				floorMaps[mapIndex].m_rawMap += line;
			}

			nMapHeight++;
		}

		floorMaps[0].CreateMapData();
		floorMaps[1].CreateMapData();
		floorMaps[2].CreateMapData();

		auto middleFloor = new ConFloor(floorMaps[1]);

		middleFloor->LinkLower(new ConFloor(floorMaps[0]));
		middleFloor->LinkUpper(new ConFloor(floorMaps[2]));

		m_cfBaseFloor = middleFloor->m_cfLower;
	}
};

class ConStageManager
{
public:
	vector<Stage> m_StageDatas;

	ConStageManager()
	{
		std::string path_to_folder = "Resources";

		for (const filesystem::directory_entry &entry : filesystem::directory_iterator(path_to_folder)) {
			if (entry.is_regular_file()) {
				Stage newStage = Stage(entry);

				m_StageDatas.push_back(newStage);
			}
		}
	};
};

