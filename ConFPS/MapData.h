#pragma once
#include <map>
#include <set>

using namespace std;

enum MapTileType
{
	None = '.',
	Door = 'D',
	Wall = '#',
	PickUpObject = 'O'
};

class MapTile
{
public:
	float m_nHeight = 0;
	MapTileType m_Type = MapTileType::None;
	std::pair<int, int> m_position;

	MapTile()
	{}

	MapTile(float height, MapTileType type, std::pair<int, int> position)
	{
		m_nHeight = height;
		m_Type = type;
		m_position = position;
	}
};

class Map
{
public:
	wstring m_rawMap;
	int m_nWidth;
	int m_nHeight;
	MapTile m_MapTiles[512];

	// Doors
	//std::map<int, MapTile*> sDoors = {};
	MapTile m_Door;
	MapTile m_PickUp;

	Map()
	{
		m_rawMap = L"";
	}

	Map(int width, int height, wstring mapData)
	{
		wstring m_rawMap = mapData;
		int m_nWidth = width;
		int m_nHeight = height;

		CreateMapData();
	}

	void CreateMapData()
	{
		//m_MapTiles = new MapTile[m_rawMap.size()];

		for (int index = 0; index < m_rawMap.size(); index++)
		{
			if (m_rawMap[index] == '#')
			{
				int x = index % m_nWidth;
				std::pair<int, int> position = std::pair<int, int>(x, (index - x) / m_nWidth);

				m_MapTiles[index] = MapTile(wallHeight, MapTileType::Wall, position);
			}
			else if (m_rawMap[index] == 'D')
			{
				int x = index % m_nWidth;
				std::pair<int, int> position = std::pair<int, int>(x, (index - x) / m_nWidth);

				MapTile door = MapTile(doorHeight, MapTileType::Door, position);
				m_Door = door;
				m_MapTiles[index] = m_Door;
			}
			else if (m_rawMap[index] == 'O')
			{
				int x = index % m_nWidth;
				std::pair<int, int> position = std::pair<int, int>(x, (index - x) / m_nWidth);

				MapTile fireBall = MapTile(12, MapTileType::PickUpObject, position);
				m_PickUp = fireBall;
				m_MapTiles[index] = fireBall;
			}
		}
	}

private:
	float wallHeight = 10;
	float doorHeight = 10;
};