#pragma once
#include "MapData.h"

using namespace std;

class ConFloor
{
public:
	ConFloor* m_cfUpper = nullptr;
	ConFloor* m_cfLower = nullptr;
	Map m_map;

	ConFloor()
	{
		m_cfUpper = nullptr;
		m_cfLower = nullptr;
		m_map = Map();
	};

	ConFloor(Map map)
	{
		m_map = map;
	}

	void LinkUpper(ConFloor* newFloor)
	{
		if (m_cfUpper != newFloor)
			m_cfUpper = newFloor;
	
		if (newFloor->m_cfLower != this)
			newFloor->LinkLower(this);
	}

	void LinkLower(ConFloor* newFloor)
	{
		if (m_cfLower != newFloor)
			m_cfLower = newFloor;

		if (newFloor->m_cfUpper != this)
			newFloor->LinkUpper(this);
	}
};

