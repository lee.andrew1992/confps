#include "ConFPS.h"

using namespace std;


int main()
{
	const int nFontWidth = 4;
	const int nFontHeight = 4;

	ConFPS game = ConFPS(nFontWidth, nFontHeight);
	game.ConstructConsole(game.GetUserScreenResolution().first / nFontWidth, game.GetUserScreenResolution().second / nFontHeight, nFontWidth, nFontHeight);
	game.SetConsoleTextCursorVisible(false);
	game.SetMouseCursorLock(game.GetUserScreenResolution().first / 2, game.GetUserScreenResolution().second);;
	game.Start();

	return 0;
}