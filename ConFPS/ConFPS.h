#pragma once
#include "ConGameEngine.h"
#include "ConEnums.h"
#include "ConFloor.h"
#include "ConStageManager.h"
#include <cmath>
#include <map>
#include <unordered_set>

class ConFPS : public ConGameEngine
{
	struct SimpleObject
	{
		float x;
		float y;
		ConSprite* sprite;
	};

private:
	Map m_currentMap;
	POINT pCursorDefault;
	list<SimpleObject> simpleObjects;
	float *fDepthBuffer;
public:
	ConFPS()
	{
		m_sAppName = L"Console FPS";
	}

	ConFPS(int nFontWidth, int nFontHeight)
	{
		m_sAppName = L"Console FPS";
		m_fontWidth = nFontWidth;
		m_fontHeight = nFontHeight;
	}

	virtual bool OnUserCreate()
	{
		// Set up the map
		ConStageManager mStageManager = ConStageManager();
		m_currentMap = mStageManager.m_StageDatas[0].m_cfBaseFloor->m_map;
		
		ConSprite* fireBallPickUpSprite = new ConSprite(L"Resources/Sprites/fps_fireball1.spr");
		simpleObjects = {
			SimpleObject { (float)m_currentMap.m_PickUp.m_position.first + 0.5f, (float)m_currentMap.m_PickUp.m_position.second + 0.5f, fireBallPickUpSprite }
		};

		fDepthBuffer = new float[ScreenWidth()];

		SetFullScreen(true);

		m_player.fMoveSpeed = 5.0f;
		m_player.fPosX = 4.0f;
		m_player.fPosY = 4.0f;
		m_player.fTurnSpeed = 1.2f;

		SetMouseCursorLock(GetUserScreenResolution().first * 0.99f, GetUserScreenResolution().second * 0.99f);
		GetCursorPos(&(pCursorDefault));

		return true;
	}

	virtual bool OnUserUpdate(float fElapsedTime)
	{
		PlayerControls(fElapsedTime);	
#pragma region DrawScreen
		for (int x = 0; x < ScreenWidth(); x++)
		{
			float fRayAngle = (m_player.fRotA - fFoV / 2.0f) + ((float)x / (float)ScreenWidth()) * fFoV;
			float fDistanceToWall = 0;
			bool bHitWall = false;

			// Wall boundary
			bool bBoundary = false;

			// Unit Vector for ray in player space
			float fEyeX = sinf(fRayAngle);
			float fEyeY = cosf(fRayAngle);

			MapTile* currentMapTile = nullptr;
			while (!bHitWall && fDistanceToWall < fDepth)
			{
				fDistanceToWall += 0.1f;

				int nCheckX = (int)(m_player.fPosX + fEyeX * fDistanceToWall);
				int nCheckY = (int)(m_player.fPosY + fEyeY * fDistanceToWall);

				currentMapTile = &(m_currentMap.m_MapTiles[nCheckY * m_currentMap.m_nWidth + nCheckX]);
				// Test if ray is out of bounds
				if (nCheckX < 0 || nCheckX >= m_currentMap.m_nWidth || nCheckY < 0 || nCheckY >= m_currentMap.m_nHeight)
				{
					// Just set distance to max depth
					bHitWall = true;
					fDistanceToWall = fDepth;
				}
				else
				{
					// Ray is in bounds so check if ray has hit a wall
					if (currentMapTile->m_Type == MapTileType::Wall || currentMapTile->m_Type == MapTileType::Door)
					{
						bHitWall = true;

						// shading the wall edges
						// vector that will hold the 4 corners of each wall
						vector<pair<float, float>> vWallCorners; // distance, dot product
						for (int xIndex = 0; xIndex < 2; xIndex++) // X corners
						{
							for (int yIndex = 0; yIndex < 2; yIndex++) // Y Corners
							{
								// vector from each perfect corners
								float vectorY = (float)nCheckY + yIndex - m_player.fPosY;
								float vectorX = (float)nCheckX + xIndex - m_player.fPosX;

								float magnitude = sqrt(vectorX * vectorX + vectorY * vectorY);
								float dotProduct = (fEyeX * vectorX / magnitude) + (fEyeY * vectorY / magnitude);
								vWallCorners.push_back(make_pair(magnitude, dotProduct));
							}

							// Sort pairs from closest to farthest
							sort(vWallCorners.begin(), vWallCorners.end(), [](const pair<float, float>& left, const pair<float, float>& right)
								{
									// the first here means the magnitude of the vWallCorners
									return left.first < right.first;
								});

							if (acos(vWallCorners.at(0).second) < fBound) bBoundary = true;
							//if (acos(vWallCorners.at(1).second) < fBound) bBoundary = true;
						}

						// Fix fish eye
						float fAngleDiff = m_player.fRotA - fRayAngle;
						if (fAngleDiff < 0)
						{
							fAngleDiff += 2 * fPi;
						}
						if (fAngleDiff > 2 * fPi)
						{
							fAngleDiff -= 2 * fPi;
						}
						fDistanceToWall = fDistanceToWall * cos(fAngleDiff);
					}
				}
			}

			int nCeiling = ((float)(ScreenHeight() / 2.0)) - ScreenHeight() / ((float)fDistanceToWall) - (currentMapTile->m_nHeight * ((float)fDistanceToWall) / fDepth);
			int nFloor = ScreenHeight() - nCeiling + (currentMapTile->m_nHeight * ((float)fDistanceToWall) / fDepth);

			// update depthbuffer
			fDepthBuffer[x] = fDistanceToWall;

			// Add shade for farther walls
			short nWallShade = ' '; // Outside vision range

			if (fDistanceToWall <= fDepth / 4.0f)			nWallShade = 0x2588; // Very Close
			else if (fDistanceToWall <= fDepth / 3.0f)		nWallShade = 0x2593;
			else if (fDistanceToWall <= fDepth / 2.0f)		nWallShade = 0x2592;
			else if (fDistanceToWall <= fDepth)				nWallShade = 0x2591; // Very Far
			else											nWallShade = ' ';

			if (bBoundary)
			{
				nWallShade = '|';
			}

			for (int y = 0; y < ScreenHeight(); y++)
			{
				if (y < nCeiling + fVerticalLookOffset)
				{
					short nCeilingShade = 0x2588;
					short nCeilingColor = BG_BLACK;

					// Simulate Looking up
					if (fVerticalLookOffset > 0)
					{
						if (fVerticalLookOffset < fVerticalLookOffsetLimit + 1)
						{
							float fCeilingDistance = (((ScreenHeight() * (fVerticalLookOffset / fVerticalLookOffsetLimit) / 2) - (float)y) * ((fVerticalLookOffset) / (fVerticalLookOffsetLimit))) * fCeilingEffect;
							if (fCeilingDistance > 6.0f)			nCeilingShade = '#';
							else if (fCeilingDistance > 2.5f)		nCeilingShade = 'x';
							else if (fCeilingDistance > 1.5f)		nCeilingShade = '-';
							else if (fCeilingDistance > 0.3f)		nCeilingShade = '.';
						}
					}

					if (nCeilingShade != 0x2588)
					{
						nCeilingColor = 0x000F;
					}

					// Draw Ceiling
					Draw(x, y, nCeilingShade, nCeilingColor);
				}
#pragma region DrawWalls
				else if (y > nCeiling + fVerticalLookOffset && y <= nFloor + fVerticalLookOffset)
				{
					if (currentMapTile->m_Type == MapTileType::Door)
					{
						Draw(x, y, 'X', FG_CYAN);
					}
					else
					{
						Draw(x, y, nWallShade);
					}
				}
#pragma endregion
#pragma region DrawFloor
				else
				{
					// floor shading
					short nFloorShade = '.';
					float fFloorDistance = 1.0f - (((float)y - ScreenHeight() / 2.0f) / ((float)ScreenHeight() / 2.0f));

					if (fFloorDistance < 0.25f * (1 - fVerticalLookOffset / 100))			nFloorShade = '#';
					else if (fFloorDistance < 0.5f * (1 - fVerticalLookOffset / 100))		nFloorShade = 'x';
					else if (fFloorDistance < 0.75f * (1 - fVerticalLookOffset / 100))		nFloorShade = '-';
					else if (fFloorDistance < 0.9f * (1 - fVerticalLookOffset / 100))		nFloorShade = '.';

					Draw(x, y, nFloorShade);

				}
#pragma endregion
			}
		}

		DrawObjects();
		DrawMap(2);
		DrawCrosshair();
#pragma endregion DrawScreen
		return true;
	}

private:
	void DrawCrosshair()
	{
		Draw((ScreenWidth() / 2) - 1, ScreenHeight() / 2, 'X', FG_RED);
		Draw((ScreenWidth() / 2) + 1, ScreenHeight() / 2, 'X', FG_RED);
		Draw(ScreenWidth() / 2, ScreenHeight() / 2, 'X', FG_RED);
		Draw(ScreenWidth() / 2, (ScreenHeight() / 2) - 1, 'X', FG_RED);
		Draw(ScreenWidth() / 2, (ScreenHeight() / 2) + 1, 'X', FG_RED);
	}


	float GetObjectAngleFromPlayer(float posX, float posY)
	{
		// Vector to Player
		float fVecX = posX - m_player.fPosX;
		float fVecY = posY - m_player.fPosY;

		// angle check
		float fEyeX = sinf(m_player.fRotA);
		float fEyeY = cosf(m_player.fRotA);
		return  atan2f(fEyeY, fEyeX) - atan2f(fVecY, fVecX);
	}

	bool IsInPlayerFoV(float posX, float posY)
	{
		float fObjectAngle = GetObjectAngleFromPlayer(posX, posY);
		if (fObjectAngle < -fPi)
		{
			fObjectAngle += 2.0f * fPi;
		}
		else if (fObjectAngle > fPi)
		{
			fObjectAngle -= 2.0f * fPi;
		}

		return fabs(fObjectAngle) < fFoV / 2.0f;
	}

	void DrawObjects()
	{
		for (SimpleObject& simpleObject : simpleObjects)
		{
			float fVecX = simpleObject.x - m_player.fPosX;
			float fVecY = simpleObject.y - m_player.fPosY;
			float fDistFromPlayer = sqrtf(fVecX * fVecX + fVecY * fVecY);

			bool bInPlayerFoV = IsInPlayerFoV(simpleObject.x, simpleObject.y);
			if (bInPlayerFoV && fDistFromPlayer >= 0.5f && fDistFromPlayer < fDepth)
			{

				float fObjectCeiling = ((float)(ScreenHeight() / 2.0)) - ScreenHeight() / ((float)fDistFromPlayer) - (1 * ((float)fDistFromPlayer) / fDepth);
				float fObjectFloor = ScreenHeight() - fObjectCeiling;
				float fObjectHeight = fObjectFloor - fObjectCeiling;

				float fObjectAspectRatio = (float)simpleObject.sprite->nHeight / (float)simpleObject.sprite->nWidth;
				float fObjectWidth = fObjectHeight / fObjectAspectRatio;

				float fObjectAngle = GetObjectAngleFromPlayer(simpleObject.x, simpleObject.y);
				float fMiddleOfObject = ((0.5f * (fObjectAngle) / (fFoV / 2.0f)) + 0.5f) * (float)ScreenWidth();

				for (float lx = 0; lx < fObjectWidth; lx++)
				{
					for (float ly = 0; ly < fObjectHeight; ly++)
					{
						float fSampleX = lx / fObjectWidth;
						float fSampleY = ly / fObjectHeight;

						wchar_t c = simpleObject.sprite->SampleGlyph(fSampleX, fSampleY);
						int nObjectColumn = (int)(fMiddleOfObject + lx - (fObjectWidth / 2.0f));
						if (nObjectColumn >= 0 && nObjectColumn < ScreenWidth())
						{
							if (c != L' ' && fDepthBuffer[nObjectColumn] >= fDistFromPlayer)
							{
								Draw(nObjectColumn, fObjectCeiling + ly + fVerticalLookOffset, c, simpleObject.sprite->SampleColour(fSampleX, fSampleY));
							}
						}
					}
				}
			}
		}
	}

	void PickUpObject(int mapPos)
	{
		m_currentMap.m_MapTiles[mapPos].m_Type = MapTileType::None;
		simpleObjects.clear();
		std::pair doorPosition = (&m_currentMap)->m_Door.m_position; 
		m_currentMap.m_MapTiles[doorPosition.second * m_currentMap.m_nWidth + doorPosition.first].m_Type = MapTileType::None;
	}

	void DrawMap(int nMapSize)
	{
		int mapSizeX = m_currentMap.m_nWidth * nMapSize;
		int mapSizeY = m_currentMap.m_nHeight * nMapSize;

		// Display Map
		for (int nMapX = 0; nMapX < mapSizeX; nMapX++)
		{
			for (int nMapY = 0; nMapY < mapSizeY; nMapY++)
			{
				Draw(nMapX + 1, nMapY + 1, m_currentMap.m_MapTiles[(nMapY / nMapSize) * m_currentMap.m_nWidth + (nMapX / nMapSize)].m_Type);
			}
		}

		Draw(1 + m_player.fPosX * nMapSize, 1 + m_player.fPosY * nMapSize, L'P', FG_YELLOW);
	}

	void PlayerControls(float fElapsedTime)
	{
		if (GetKey(L'A').bHeld)
		{
			m_player.fPosX -= cosf(m_player.fRotA) * m_player.fMoveSpeed * fElapsedTime;
			m_player.fPosY += sinf(m_player.fRotA) * m_player.fMoveSpeed * fElapsedTime;

			int playerMapPos = (int)m_player.fPosY * m_currentMap.m_nWidth + (int)m_player.fPosX;
			if (m_currentMap.m_MapTiles[playerMapPos].m_Type == MapTileType::PickUpObject)
			{
				PickUpObject(playerMapPos);
			}

			else if (m_currentMap.m_MapTiles[playerMapPos].m_Type == MapTileType::Wall ||
				m_currentMap.m_MapTiles[playerMapPos].m_Type == MapTileType::Door)
			{
				m_player.fPosX += cosf(m_player.fRotA) * m_player.fMoveSpeed * fElapsedTime;
				m_player.fPosY -= sinf(m_player.fRotA) * m_player.fMoveSpeed * fElapsedTime;
			}
		}


		if (GetKey(L'D').bHeld)
		{
			m_player.fPosX += cosf(m_player.fRotA) * m_player.fMoveSpeed * fElapsedTime;
			m_player.fPosY -= sinf(m_player.fRotA) * m_player.fMoveSpeed * fElapsedTime;
			int playerMapPos = (int)m_player.fPosY * m_currentMap.m_nWidth + (int)m_player.fPosX;

			if (m_currentMap.m_MapTiles[playerMapPos].m_Type == MapTileType::PickUpObject)
			{
				PickUpObject(playerMapPos);
			}

			else if (m_currentMap.m_MapTiles[playerMapPos].m_Type == MapTileType::Wall ||
				m_currentMap.m_MapTiles[playerMapPos].m_Type == MapTileType::Door)
			{
				m_player.fPosX -= cosf(m_player.fRotA) * m_player.fMoveSpeed * fElapsedTime;
				m_player.fPosY += sinf(m_player.fRotA) * m_player.fMoveSpeed * fElapsedTime;
			}
		}

		if (GetKey(L'W').bHeld)
		{
			m_player.fPosX += sinf(m_player.fRotA) * m_player.fMoveSpeed * fElapsedTime;
			m_player.fPosY += cosf(m_player.fRotA) * m_player.fMoveSpeed * fElapsedTime;
			int playerMapPos = (int)m_player.fPosY * m_currentMap.m_nWidth + (int)m_player.fPosX;

			if (m_currentMap.m_MapTiles[playerMapPos].m_Type == MapTileType::PickUpObject)
			{
				PickUpObject(playerMapPos);
			}

			else if (m_currentMap.m_MapTiles[playerMapPos].m_Type == MapTileType::Wall ||
				     m_currentMap.m_MapTiles[playerMapPos].m_Type == MapTileType::Door)
			{
				m_player.fPosX -= sinf(m_player.fRotA) * m_player.fMoveSpeed * fElapsedTime;
				m_player.fPosY -= cosf(m_player.fRotA) * m_player.fMoveSpeed * fElapsedTime;
			}
		}

		if (GetKey(L'S').bHeld)
		{
			m_player.fPosX -= sinf(m_player.fRotA) * m_player.fMoveSpeed * fElapsedTime;
			m_player.fPosY -= cosf(m_player.fRotA) * m_player.fMoveSpeed * fElapsedTime;
			int playerMapPos = (int)m_player.fPosY * m_currentMap.m_nWidth + (int)m_player.fPosX;

			if (m_currentMap.m_MapTiles[playerMapPos].m_Type == MapTileType::PickUpObject)
			{
				PickUpObject(playerMapPos);
			}

			else if (m_currentMap.m_MapTiles[playerMapPos].m_Type == MapTileType::Wall ||
					 m_currentMap.m_MapTiles[playerMapPos].m_Type == MapTileType::Door)
			{
				m_player.fPosX += sinf(m_player.fRotA) * m_player.fMoveSpeed * fElapsedTime;
				m_player.fPosY += cosf(m_player.fRotA) * m_player.fMoveSpeed * fElapsedTime;
			}
		}

		// Mouse Controls
		POINT pCursor;
		if (GetCursorPos(&pCursor))
		{
			long deltaCursorPositionX = pCursorDefault.x - pCursor.x;
			if (deltaCursorPositionX > 0)
			{
				m_player.fRotA -= m_player.fTurnSpeed * fElapsedTime;
			}
			if (deltaCursorPositionX < 0)
			{
				m_player.fRotA += m_player.fTurnSpeed * fElapsedTime;
			}

			long deltaCursorPositionY = pCursorDefault.y - pCursor.y;
			if (abs(deltaCursorPositionY) > 2.0f) // input deadzone
			{
				if (deltaCursorPositionY > 0)
				{
					fVerticalLookOffset = min(fVerticalLookOffsetLimit, fVerticalLookOffset += m_player.fTurnSpeed * fElapsedTime * 75);
				}
				if (deltaCursorPositionY < 0)
				{
					fVerticalLookOffset = max(-fVerticalLookOffsetLimit, fVerticalLookOffset -= m_player.fTurnSpeed * fElapsedTime * 75);
				}
			}

			SetMouseCursorLock(GetUserScreenResolution().first * 0.99f, GetUserScreenResolution().second * 0.99f);
		}
	}

public:
	Map GetMap() { return m_currentMap; }

private:
	float fPi = 3.14159f;
	// field of view
	float fFoV = fPi * 0.35f;
	// limit to vision distance
	float fDepth = 16.0f;

	// wall boundary width
	float fBound = 0.008f;

	// drawing ceiling should be slowed down
	float fCeilingEffect = 0.3f;

	// Used to simulate movement between elevation
	float fVerticalLookOffset = 0;
	float fVerticalLookOffsetLimit = 175.0f;

	int nFontWidth;
	int nFontHeight;

	// TEST VALUES:
	float testValue = 0;
};
