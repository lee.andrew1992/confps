# ConFPS
Passion project

It is an extension of javidx9's Command Prompt FPS tutorials
https://youtu.be/xW8skO7MFYw?si=16iewm-NWopR4p8K

For example ConGameEngine.h is mainly his code, but with some of my own functions added
I'll be continuing to add more code as I see fit to reach my goal for this project which is to be a retro FPS game similar to Heretic:
https://store.steampowered.com/app/2390/Heretic_Shadow_of_the_Serpent_Riders/

But with just ASCII graphics